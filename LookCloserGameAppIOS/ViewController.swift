//
//  ViewController.swift
//  LookCloserGameAppIOS
//
//  Created by duuple duuple on 2.10.2019.
//  Copyright © 2019 Demarp Software. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration

class ViewController: UIViewController {

    @IBOutlet weak var myWebview: UIWebView!
    
    @IBOutlet weak var warningIcon: UIImageView!
    @IBOutlet weak var warningText: UILabel!
    @IBOutlet weak var warningButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WarningHide()
        
        LoadWebPage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setGradientBackground()
        super.viewWillAppear(animated)
    }
    
    func LoadWebPage() -> Void {
        if (isInternetAvailable()) {
            WarningHide()
            let url = URL(string: "http://www.lookcloser.net/mobile")
            myWebview.loadRequest(URLRequest(url: url!))
        } else {
            WarningShow()
        }
    }
    
    @IBAction func buttonTryAgain(_ sender: Any) {
        if (isInternetAvailable()) {
            WarningHide()
            let url = URL(string: "http://www.lookcloser.net/mobile")
            myWebview.loadRequest(URLRequest(url: url!))
        } else {
            showToast(message: "Check your internet connection.")
            WarningShow()
        }
    }
    
    func WarningHide() -> Void {
        warningIcon.isHidden = true
        warningText.isHidden = true
        warningButton.isHidden = true
        myWebview.isHidden = false
    }
    
    func WarningShow() -> Void {
        warningIcon.isHidden = false
        warningText.isHidden = false
        warningButton.isHidden = false
        myWebview.isHidden = true
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 254.0/255.0, green: 196.0/255.0, blue: 61.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 254.0/255.0, green: 162.0/255.0, blue: 23.0/255.0, alpha: 1.0).cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds

        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

